﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG2
{
    /// <summary>
    /// A static class with helper methods
    /// </summary>
    public class KeyboardHelper
    {
        private const int LINE_BREAKER_LENGTH = 40;
        private const char LINE_BREAKER_CHAR = '*';
        
        private ConsoleColor _previousColor = ConsoleColor.Gray;
        private ConsoleColor _currentColor = ConsoleColor.Gray;

        private ConsoleColor _selectedColor = ConsoleColor.White;

        private const string DEFAULT_BAD_INPUT_FEEDBACK = "Please enter valid input";

        public void ClearDisplay()
        {
            Console.Clear();
        }

        public void LineBreaker()
        {
            Console.WriteLine("".PadRight(LINE_BREAKER_LENGTH, LINE_BREAKER_CHAR));
        }

        public void SetTextColor(ConsoleColor c)
        {
            _previousColor = _currentColor;
            _currentColor = c;
            Console.ForegroundColor = c;
        }

        #region Write and get input

        public void WriteText(string msg, ConsoleColor c, int linesToSkip = 1)
        {
            Console.ForegroundColor = c;
            Console.Write(msg);
            Console.ForegroundColor = _currentColor;
            while (linesToSkip-- > 0)
            {
                GoNextLine();
            }
        }

        public void WriteText(string msg, int linesToSkip = 1)
        {
            WriteText(msg, _currentColor, linesToSkip);
        }

        public string GetFeedback(string feedback, string inputFormat, bool ensureNotEmpty = true)
        {
            if (!ensureNotEmpty) return Console.ReadLine();
            string msg = Console.ReadLine();
            while (msg.Trim().Equals(""))
            {
                GoNextLine();
                WriteText(inputFormat, 1);
                WriteText(feedback, 0);
                msg = Console.ReadLine();
            }
            return msg;
        }

        public string GetFeedback(string feedback, bool ensureNotEmpty = true)
        {
            return GetFeedback(feedback, DEFAULT_BAD_INPUT_FEEDBACK, ensureNotEmpty);
        }

        public string GetFeedback(bool ensureNotEmpty = true)
        {
            return GetFeedback("", DEFAULT_BAD_INPUT_FEEDBACK, ensureNotEmpty);
        }

        public void GoNextLine()
        {
            Console.WriteLine();
        }

        #endregion

        #region Options

        /// <summary>
        /// Allows the user to select ONE item in a list
        /// </summary>
        /// <param name="options">A string of options, put the last option as the 'special' item if applicable</param>
        /// <param name="hasSpecialLastOption">Set this to true if the last item is special, ie exit</param>
        /// <returns></returns>
        public int PickOption(List<string> options, bool hasSpecialLastOption = false)
        {
            bool wasVis = Console.CursorVisible;
            int currentPos = Console.CursorTop;
            int selectedIndex = 0;
            int lastindex = 0;
            Console.CursorVisible = false;
            int length = options.Count;

            for (int i = 0; i < length; i++)
            {
                if (hasSpecialLastOption && i + 1 == length)
                {
                    GoNextLine();
                }
                WriteText(getOptionString(i, length, hasSpecialLastOption, options[i]),
                    (i == 0) ? _selectedColor : _currentColor);
            }

            while (true)
            {
                if (Console.KeyAvailable)
                {
                    lastindex = selectedIndex;
                    ConsoleKey k = Console.ReadKey(true).Key;
                    if (k == ConsoleKey.Enter)
                    {
                        Console.CursorVisible = wasVis;
                        Console.CursorTop = currentPos + options.Count + ((hasSpecialLastOption) ? 1 : 0);
                        return selectedIndex;
                    }
                    else if (k == ConsoleKey.DownArrow)
                    {
                        selectedIndex++;
                    }
                    else if (k == ConsoleKey.UpArrow)
                    {
                        selectedIndex--;
                    }
                    else
                    {
                        selectedIndex = checkNumbers(selectedIndex, length, hasSpecialLastOption, k);
                    }

                    //wraps around
                    if (selectedIndex < 0) selectedIndex = options.Count - 1;
                    else if (selectedIndex > options.Count - 1) selectedIndex = 0;

                    Console.CursorTop = currentPos + getCursorPosition(lastindex, length, hasSpecialLastOption);
                    WriteText(getOptionString(lastindex, length, hasSpecialLastOption, options[lastindex]), _currentColor);
                    Console.CursorTop = currentPos + getCursorPosition(selectedIndex, length, hasSpecialLastOption);
                    WriteText(getOptionString(selectedIndex, length, hasSpecialLastOption, options[selectedIndex]), _selectedColor);
                }
            }
           
        }

        private int checkNumbers(int ind, int length, bool specialLast, ConsoleKey pressedKey)
        {
            int start = (specialLast) ? 0 : 1;
            int pressedNum = -1;
            for (int i = start; i <= Math.Min(length, 9); i++)
            {
                ConsoleKey tmp = (ConsoleKey)(48 + i);
                if (pressedKey.Equals(tmp))
                {
                    pressedNum = i;
                }
            }
            if (pressedNum != -1)
            {
                if (pressedNum == 0)
                {
                    return length - 1;
                } else
                {
                    return pressedNum - 1;
                }
            }
            return ind;
        }

        private int getCursorPosition(int ind, int length, bool specialLast)
        {
            if (!specialLast)
            {
                return ind;
            }
            if (ind + 1 == length)
            {
                // potentially ind + 1 if we end up having a blank line between this
                return ind + 1; 
            }
            return ind;
        }

        private string getOptionString(int ind, int length, bool specialLast, string str)
        {
            if (ind == length - 1 && specialLast)
            {
                return "0: " + str;
            }
            return (ind + 1).ToString() + ": " + str;
        }

        #endregion

        public void EnterToContinue(bool clear = true)
        {
            LineBreaker();
            Console.WriteLine("Press enter to continue...");
            while (true)
            {
                if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Enter)
                {
                    break;
                }
            }
            
            if (clear)
            {
                ClearDisplay();
            }
        }

    }
}
