﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG2.GameStates
{
    public class GameStateManager
    {

        #region Variables and Properties

        private GameState _currentState;

        public GameState CurrentState
        {
            get { return _currentState; }
        }

        #endregion

        #region Constructor

        #endregion

        #region Methods

        public void Transition(GameState nextState)
        {
            if (_currentState == null || !_currentState.Equals(nextState))
            {
                if (_currentState != null) _currentState.OnClose();
                _currentState = nextState;
                nextState.Refresh();
                nextState.Begin();
            }
        }

        #endregion
    }
}
