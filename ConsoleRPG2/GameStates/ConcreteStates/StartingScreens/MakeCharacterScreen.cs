﻿using ConsoleRPG2.Entities.ConcreteEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG2.GameStates.ConcreteStates.StartingScreens
{
    public class MakeCharacterScreen : GameState
    {


        #region Variables and Properties

        #endregion

        #region Constructor

        public MakeCharacterScreen(Main main) : base(main)
        {

        }

        #endregion


        #region Methods

        public override void Begin()
        {
            _kh.WriteText("Please enter the name of your character", ConsoleColor.White, 2);
            string name = null;
            while (true)
            {
                _kh.WriteText("Name: ", 0);
                name = _kh.GetFeedback("Name: ");
                if (_mainRef.ContainsPlayerName(name))
                {
                    _kh.WriteText(name + " already exists!");
                }
                else
                {
                    break;
                }
            }
            _kh.WriteText(name + " has been created! Please load your character through the main menu");
            Player player = new Player(name);
            player.Save();
            _kh.EnterToContinue();
            _mainRef.StateManager.Transition(_mainRef.MenuScreen);
        }

        public override void Refresh()
        {
            base.Refresh();
            _kh.ClearDisplay();
        }

        #endregion


    }
}
