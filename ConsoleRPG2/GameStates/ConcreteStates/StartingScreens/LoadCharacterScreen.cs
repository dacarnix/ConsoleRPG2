﻿using ConsoleRPG2.Entities.ConcreteEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG2.GameStates.ConcreteStates.StartingScreens
{
    public class LoadCharacterScreen : GameState
    {


        #region Variables and Properties

        #endregion

        #region Constructor

        public LoadCharacterScreen(Main main) : base(main)
        {

        }

        #endregion


        #region Methods

        public override void Begin()
        {
            _kh.WriteText("Pick the hero of your character", ConsoleColor.White, 2);

            List<string> choices = _mainRef.GetPlayerNames();
            choices.Add("Menu");

            _kh.PickOption(choices, true);

            _kh.EnterToContinue();
            _mainRef.StateManager.Transition(_mainRef.MenuScreen);
        }

        public override void Refresh()
        {
            base.Refresh();
            _kh.ClearDisplay();
        }

        #endregion


    }
}
