﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG2.GameStates.ConcreteStates
{
    public class MenuScreen : GameState
    {


        #region Variables and Properties

        #endregion

        #region Constructor

        public MenuScreen(Main main) : base(main)
        {

        }

        #endregion


        #region Methods

        public override void Begin()
        {
            _kh.WriteText("Welcome to the official reboot of ConsoleRPG: ConsoleRPG 2!", ConsoleColor.White, 2);
            _kh.WriteText("What would you like to do today?");
            List<Action> actions = new List<Action>();
            List<string> options = new List<string>();
            options.Add("Make a new hero");
            actions.Add(() => _mainRef.StateManager.Transition(_mainRef.MakeCharacter));

            if (_mainRef.PlayersExist())
            {
                options.Add("Load an existing hero");
                actions.Add(() => _mainRef.StateManager.Transition(_mainRef.LoadScreen));
            }
            options.Add("What is this game?");
            actions.Add(() => Console.WriteLine("Not Implemented"));
            options.Add("Exit");
            actions.Add(() => _mainRef.Exit());
            int indexSelected = _kh.PickOption(options, true);
            actions[indexSelected].Invoke();
        }

        public override void Refresh()
        {
            base.Refresh();
            _kh.ClearDisplay();
            _mainRef.LoadPlayers();
        }

        #endregion

        
    }
}
