﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG2.GameStates
{
    public abstract class GameState
    {
        #region Variables and Properties
        protected KeyboardHelper _kh;
        protected Main _mainRef;

        #endregion

        #region Constructor

        public GameState(Main main)
        {
            this._mainRef = main;
            _kh = new KeyboardHelper();
        }

        #endregion

        #region Methods

        public abstract void Begin();

        public virtual void OnClose()
        {

        }

        public virtual void Refresh()
        {

        }

        #endregion

    }
}
