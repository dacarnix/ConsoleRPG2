﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG2.Entities
{
    /// <summary>
    /// Parent class for all entities
    /// </summary>
    [Serializable]
    public abstract class Entity
    {
        #region Variables and Properties

        protected int _health, _maxHealth;
        protected string _name;

        public string Name { get { return _name; } }

        #endregion

        #region Constructor

        public Entity()
        {

        }

        #endregion

        #region Methods

        #endregion
    }
}
