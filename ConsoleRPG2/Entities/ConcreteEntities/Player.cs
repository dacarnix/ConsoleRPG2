﻿using ConsoleRPG2.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG2.Entities.ConcreteEntities
{
    [Serializable]
    public class Player : Entity
    {

        #region Variables and Properties

        #endregion

        #region Constructor

        public Player(string name) : base()
        {
            base._name = name;
        }

        #endregion

        #region Methods

        #endregion

        #region Save

        /// <summary>
        /// Saves the player to the name
        /// </summary>
        public void Save()
        {
            string path = Constants.PLAYER_SAVE_PATH + _name + Constants.PLAYER_SAVE_EXTENSION;
            IOExtensions.WriteToBinaryFile<Player>(path, this);
        }


        #endregion
    }
}
