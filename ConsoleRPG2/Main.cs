﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleRPG2.GameStates;
using ConsoleRPG2.GameStates.ConcreteStates;
using System.IO;
using ConsoleRPG2.Entities.ConcreteEntities;
using ConsoleRPG2.IO;
using ConsoleRPG2.GameStates.ConcreteStates.StartingScreens;

namespace ConsoleRPG2
{
    /// <summary>
    /// The main entry loop of the application, is called from Program.cs
    /// </summary>
    public class Main
    {


        #region Variables

        private GameStateManager _stateManager;

        #region States

        private MenuScreen _menuScreen;
        private MakeCharacterScreen _makeScreen;
        private LoadCharacterScreen _loadCharacterScreen;

        #endregion

        private List<Player> _players;

        public bool PlayersExist()
        {
            return _players.Count > 0;
        }

        public bool ContainsPlayerName(string name)
        {
            foreach (Player p in _players)
            {
                if (p.Name.ToLower().Equals(name.ToLower()))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region Properties

        public GameStateManager StateManager { get { return _stateManager; } }

        public MenuScreen MenuScreen { get { return _menuScreen; } }

        public LoadCharacterScreen LoadScreen { get { return _loadCharacterScreen; } }

        public MakeCharacterScreen MakeCharacter { get { return _makeScreen; } }

        #endregion

        #region Constructor

        public Main()
        {
            CheckDirectories();

            _menuScreen = new MenuScreen(this);
            _makeScreen = new MakeCharacterScreen(this);
            _loadCharacterScreen = new LoadCharacterScreen(this);
            _stateManager = new GameStateManager();

            //Console.SetWindowSize(_width, _height);
            Console.Title = Constants.APPLICATION_NAME + " - " + Constants.VERSION;

            _players = new List<Player>();
            LoadPlayers();

            _stateManager.Transition(_menuScreen);
        }

        #endregion

        #region Directories

        private void CheckDirectories()
        {
            if (!Directory.Exists(Constants.APP_BASE_PATH))
            {
                Directory.CreateDirectory(Constants.APP_BASE_PATH);
            }
            if (!Directory.Exists(Constants.PLAYER_SAVE_PATH))
            {
                Directory.CreateDirectory(Constants.PLAYER_SAVE_PATH);
            }
        }

        /// <summary>
        /// Looks at the save location and loads any files present in to player objects
        /// </summary>
        public void LoadPlayers()
        {
            _players.Clear();
            string[] fileList = Directory.GetFiles(Constants.PLAYER_SAVE_PATH, "*" + Constants.PLAYER_SAVE_EXTENSION);
            foreach (string s in fileList)
            {
                try
                {
                    _players.Add(IOExtensions.ReadFromBinaryFile<Player>(s));
                }
                catch (Exception ex) { }
            }
        }

        #endregion

        #region Methods

        public void MainLoop()
        {
            
        }

        public void Exit()
        {
            Environment.Exit(0);
        }

        public List<string> GetPlayerNames()
        {
            List<string> info = new List<string>();
            foreach (Player p in _players)
            {
                info.Add(p.Name);
            }
            return info;
        }

        #endregion

    }
}
