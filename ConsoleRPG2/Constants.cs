﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleRPG2
{
    public static class Constants
    {
        public const string VERSION = "0.1a";
        public const string APPLICATION_NAME = "ConsoleRPG2";

        private static readonly string BASE_PATH = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public static readonly string APP_BASE_PATH = Path.Combine(BASE_PATH, APPLICATION_NAME) + Path.DirectorySeparatorChar;

        public static readonly string PLAYER_SAVE_PATH = Path.Combine(APP_BASE_PATH, "Players") + Path.DirectorySeparatorChar;
        public const string PLAYER_SAVE_EXTENSION = ".save";
    }
}
